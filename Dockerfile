# Base image for terraform binary
FROM hashicorp/terraform:1.6.6 as terraform

# Base image for packer binary
FROM hashicorp/packer:1.10.0 as packer

# Final base image
FROM amazon/aws-cli:2.15.9

# Install common dependencies
RUN set -ex && \
      yum install tar gzip curl wget git bash -y

# Install kubectl
ARG KUBECTL_VERSION=1.31.2
RUN set -ex && \
    curl -sL https://storage.googleapis.com/kubernetes-release/release/v$KUBECTL_VERSION/bin/linux/amd64/kubectl \
      > /usr/local/bin/kubectl && chmod +x /usr/local/bin/kubectl

# Install helm3
ARG HELM_VERSION=3.16.3
RUN set -ex && \
    curl -sL https://get.helm.sh/helm-v$HELM_VERSION-linux-amd64.tar.gz \
      | tar -zx -C /usr/local/bin --strip-components=1 linux-amd64/helm

# Install helm3 plugins
RUN helm plugin install https://github.com/jkroepke/helm-secrets --version v4.5.1 && \
    helm plugin install https://github.com/databus23/helm-diff --version v3.9.1 && \
    helm plugin install https://github.com/instrumenta/helm-kubeval

# Install eksctl (latest version)
RUN set -ex && \
    curl -sL "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp && \
    mv /tmp/eksctl /usr/bin && chmod +x /usr/bin/eksctl

# Install kube-linter
RUN set -ex && \
    curl -sL https://github.com/stackrox/kube-linter/releases/download/0.6.5/kube-linter-linux \
          > /usr/local/bin/kube-linter && chmod +x /usr/local/bin/kube-linter

# Install aws-iam-authenticator
RUN set -ex && \
    curl -sL https://amazon-eks.s3.us-west-2.amazonaws.com/1.21.2/2021-07-05/bin/linux/amd64/aws-iam-authenticator \
      > /usr/local/bin/aws-iam-authenticator && chmod +x /usr/local/bin/aws-iam-authenticator

# Install krew
RUN set -ex && \
    OS="$(uname | tr '[:upper:]' '[:lower:]')" && \
    ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" && \
    KREW="krew-${OS}_${ARCH}" && \
    curl -sL "https://github.com/kubernetes-sigs/krew/releases/download/v0.4.4/$KREW.tar.gz" | tar xz -C /tmp && \
    /tmp/$KREW install krew

# Install sops
RUN set -ex && \
    curl -sL https://github.com/getsops/sops/releases/download/v3.8.1/sops-v3.8.1.linux.amd64 \
      > /usr/local/bin/sops && chmod +x /usr/local/bin/sops

# Install terraform
COPY --from=terraform [ "/bin/terraform", "/usr/bin/terraform" ]

# Install packer
COPY --from=packer [ "/bin/packer", "/usr/bin/packer" ]

WORKDIR /root/workspaces

RUN mkdir -p /root/.kube/config-files

COPY [ "code/load-k8s-configs.sh", "." ]
COPY [ "files/kube-config", "/root/.kube/config-files" ]
COPY [ "files/home", "/root" ]

# Install kubectl plugins
RUN source ~/.bashrc && \
    kubectl krew install ctx && \
    kubectl krew install ns

ENTRYPOINT [ "/bin/bash" ]
