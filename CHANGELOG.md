# Semantic Versioning Changelog

## [1.5.4](https://gitlab.com/nuageit-community/cloud-toolbox/compare/1.5.3...1.5.4) (2024-01-14)


### :bug: Fixes

* update setup awscli ([d0139cf](https://gitlab.com/nuageit-community/cloud-toolbox/commit/d0139cf646d4af9420e70f44956049f1cc9339b1))

## [1.5.3](https://gitlab.com/nuageit-community/cloud-toolbox/compare/1.5.2...1.5.3) (2024-01-14)


### :bug: Fixes

* command syntax ([8529b35](https://gitlab.com/nuageit-community/cloud-toolbox/commit/8529b35970c6c5156ece8c8899793b05db485afc))

## [1.5.2](https://gitlab.com/nuageit-community/cloud-toolbox/compare/1.5.1...1.5.2) (2024-01-14)


### :bug: Fixes

* image versions ([05ce749](https://gitlab.com/nuageit-community/cloud-toolbox/commit/05ce74914384343a1b628331e1a6aa9e7d9e2f54))

## [1.5.1](https://gitlab.com/nuageit-community/cloud-toolbox/compare/1.5.0...1.5.1) (2024-01-13)


### :bug: Fixes

* dockerfile setup ([0484166](https://gitlab.com/nuageit-community/cloud-toolbox/commit/04841661fc09540ed2f907a2ebcd77abf5701ca5))

## [1.5.0](https://gitlab.com/nuageit-community/cloud-toolbox/compare/1.4.0...1.5.0) (2024-01-13)


### :zap: Refactoring

* update file name ([1b61e0a](https://gitlab.com/nuageit-community/cloud-toolbox/commit/1b61e0ae1d9abdf271968c69298d3b118bc5d332))


### :repeat: CI

* add include job ([7a7df87](https://gitlab.com/nuageit-community/cloud-toolbox/commit/7a7df873263fce38204b01eb1901a0e951984f7d))
* change include ([38e3891](https://gitlab.com/nuageit-community/cloud-toolbox/commit/38e38912f2a0b08e418968ef6fa963245ff43db2))
* change ref name ([59a5e3c](https://gitlab.com/nuageit-community/cloud-toolbox/commit/59a5e3caf633603419b6ea6ddfeb0c96e89974f4))


### :memo: Docs

* auto deploy setup ([3c78eeb](https://gitlab.com/nuageit-community/cloud-toolbox/commit/3c78eeb6f083e6d967a883d0c698b8d9627190a3))
* change script ([4798bf1](https://gitlab.com/nuageit-community/cloud-toolbox/commit/4798bf1c3f33eae707137df200c28c51cd3ece65))
* pretty readme [skip ci] ([1413141](https://gitlab.com/nuageit-community/cloud-toolbox/commit/141314177d53475c816b1cf179a221fb5b53fe3b))


### :bug: Fixes

* change vars ([1098ac1](https://gitlab.com/nuageit-community/cloud-toolbox/commit/1098ac105241fff2b7b12600389119bc60d7579f))
* gitleaks config ([c93c84c](https://gitlab.com/nuageit-community/cloud-toolbox/commit/c93c84cabd95fefcd2d884c143b8fc073b0ae2e6))
* hooks setup ([28b089d](https://gitlab.com/nuageit-community/cloud-toolbox/commit/28b089d7a2fce361b1c9e0145d13e2882bde32b5))
* identation yaml ([6f25475](https://gitlab.com/nuageit-community/cloud-toolbox/commit/6f2547576d524461863bcd23efcc397039508fed))
* organize project setup ([a959bac](https://gitlab.com/nuageit-community/cloud-toolbox/commit/a959bac89b46bda1aea0fbe109f8fb5798ce9e85))
* pre-commit and makefile ([a5756e7](https://gitlab.com/nuageit-community/cloud-toolbox/commit/a5756e7a0f26560ee9028589c0b0775e162b0f58))
* setup configurations ([083e3bf](https://gitlab.com/nuageit-community/cloud-toolbox/commit/083e3bf316265bd80eec4eebab23fab62dada325))
* setup docs and taskfile ([e39eafb](https://gitlab.com/nuageit-community/cloud-toolbox/commit/e39eafb594cc1a5a6daec793b9002f683b6d47c6))

## [1.4.0](https://gitlab.com/nuageit-community/cloud-toolbox/compare/1.3.0...1.4.0) (2023-01-17)


### :sparkles: News

* add fzf ([225ec03](https://gitlab.com/nuageit-community/cloud-toolbox/commit/225ec030b8feb716cb1e2c3c924a9eb14aa2528f))


### :bug: Fixes

* general versions update ([ef6d9bb](https://gitlab.com/nuageit-community/cloud-toolbox/commit/ef6d9bb47393f12050de9b61368a8c5d4a8a5e99))
* update pre-commit ([ed0a00d](https://gitlab.com/nuageit-community/cloud-toolbox/commit/ed0a00de2ccd409f8e4334bdcf4a4bd8b6317ae9))


### :memo: Docs

* add important note ([0fe7444](https://gitlab.com/nuageit-community/cloud-toolbox/commit/0fe744413dd16bcf33f1c2c401515a586cf79095))
* resize gif ([bd0fae2](https://gitlab.com/nuageit-community/cloud-toolbox/commit/bd0fae23d7b7ee5f3d89c570d029f27dc508a79b))


### :repeat: CI

* adicionando novo template pipeline ([88dfdf5](https://gitlab.com/nuageit-community/cloud-toolbox/commit/88dfdf56aa704d6d0f956f3aa6cf9be9672d482f))

## [1.3.0](https://gitlab.com/nuageit/devops/cloud-toolbox/compare/1.2.0...1.3.0) (2022-09-20)


### :bug: Fixes

* script and dockerfile ([4579ed9](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/4579ed9bc9ec07dc088f7261ad19bc7cbd423d62))


### :memo: Docs

* more tools in list ([83ebfa4](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/83ebfa4129c4b7dd6efab68fba0b46ef4db92d47))


### :sparkles: News

* add kube-linter ([8df03ce](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/8df03cec26e62144059901c8d1efa8b76ca646c0))

## [1.2.0](https://gitlab.com/nuageit/devops/cloud-toolbox/compare/1.1.0...1.2.0) (2022-09-19)


### :sparkles: News

* **SDS-41:** add packer cli ([018a34b](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/018a34bbe2aa62cbd01dc84ef18e194fe26e6cbd))
* **SDS-42:** add multiple kubeconfigs strategy with encryption kms+sops ([5a418c9](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/5a418c9343a5d99db5f2e9dd83c79f7a69b8e242))
* **SDS-42:** add others configs ([fdbbd65](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/fdbbd656509f86bcf56e81146c55e7c1d1fd40d8))
* **SDS-42:** add tomos kubeconfigs ([131e983](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/131e983de6dfb537e7461db191aa006b7ce451e2))
* **SDS-42:** clients list ([2bb6dc7](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/2bb6dc72457b52c940106478d56abcbe7e024606))


### :bug: Fixes

* **SDS-42:** style commands and files ([00682e1](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/00682e1161b1b0510a02c6ce9557d022a7838dbd))

## [1.1.0](https://gitlab.com/nuageit/devops/cloud-toolbox/compare/1.0.1...1.1.0) (2022-09-19)


### :bug: Fixes

* install clis in dockerfile ([8e68ab4](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/8e68ab4de2761d50f4468f5680d76b5b159d32f9))


### :memo: Docs

* add tools ([f5b042d](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/f5b042d88d1763d42da04a9e427ac4fcd88a55ec))
* more topics ([2253c2b](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/2253c2b8bce86755cb27dc561212c88532ee7d86))
* pretty readme ([46d026b](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/46d026ba2104c3c44766901bdf406d6cd42af5fb))
* resize image gif ([5ea0568](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/5ea0568a975c751263add067125ddc4a8810fa86))


### :sparkles: News

* add helm kubeval ([a6f7cc2](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/a6f7cc23706d198ceb03685e8f80d4cea1e9b48b))
* add helm plugins ([c301951](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/c301951a205c55133600cdc6ddec9767f3e8392a))
* **SDS-41:** add krew plugin ([8431032](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/8431032b41a98ebd877fd4de677d0456d4197beb))

## [1.0.1](https://gitlab.com/nuageit/devops/cloud-toolbox/compare/1.0.0...1.0.1) (2022-09-16)


### :repeat: CI

* build dind ([d7208fb](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/d7208fbe32ac408a057efb88c70e27106f3133c8))

## 1.0.0 (2022-09-16)


### :sparkles: News

* initial commit with Dockerfile ([c7d7c6d](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/c7d7c6dd578f66921bac05df0561fdd025a1c7ee))


### :repeat: CI

* add build and scan ([739ec4b](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/739ec4bea40f1be6a5c71478d55a5bee089f7233))
* add simple pipeline ([fa7ead0](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/fa7ead044b264259c73c5bab98a421d8a7ae651c))
* fix stages definition ([7a6a1d6](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/7a6a1d67dd836a85d9b04a777d0c487639bcc43c))
* workflow setup ([fd6bf19](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/fd6bf19df782ba6305bda9a09857fa1ab6d866bd))


### :bug: Fixes

* change dockerfile workdir ([ba8ba7d](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/ba8ba7db221a9a03333ecab01e019ce94bd61f41))
* test jobs ([0076b4a](https://gitlab.com/nuageit/devops/cloud-toolbox/commit/0076b4a3b6f6a96a5be1aef500954e9b71ceeb54))
